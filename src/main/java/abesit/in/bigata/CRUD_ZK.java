package abesit.in.bigata;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;


public class CRUD_ZK extends ZK_Connection {

    ZK_Connection zk_connection =new ZK_Connection() ;
    private final static Logger logger_CRUD_ZK = Logger.getLogger(CRUD_ZK.class.getName());
    private static Handler filehanlder ;
    private static ZooKeeper zkeeper;

    public ZooKeeper intialize (){

        try {

            zkeeper = zk_connection.connect("192.168.100.205:2180");
            //change ip
            System.out.println(zkeeper);
//            System.out.println("Connected to Zookeeper");
            //log
            logger_CRUD_ZK.info("connected");

            return  zkeeper;

        } catch (Exception e) {
            System.out.println(e.getMessage()); // Catches error messages
        }
        return  zkeeper ;

    }


    public void create(String path, byte[] data) throws KeeperException,
            InterruptedException {
        zkeeper.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.PERSISTENT);
        //log
        logger_CRUD_ZK.info("Znode created");

    }
    //get node status
    public Stat getZNodeStats(String path) throws KeeperException,
            InterruptedException {
        Stat stat = zkeeper.exists(path, true);
        if (stat != null) {
            System.out.println("Node exists and the node version is "
                    + stat.getVersion());
            logger_CRUD_ZK.info("node exist");
        } else {
            System.out.println("Node does not exists");
            logger_CRUD_ZK.info("node does not exists");
        }
        return stat;
    }
    // update znode data
    public void update(String path, byte[] data) throws KeeperException,
            InterruptedException {
        int version = zkeeper.exists(path, true).getVersion();
        zkeeper.setData(path, data, version);
        //log
        logger_CRUD_ZK.info("znode updated");

    }

    // Delete znode
    public void delete(String path) throws KeeperException,
            InterruptedException {
        int version = zkeeper.exists(path, true).getVersion();
        zkeeper.delete(path, version);
        //log
        logger_CRUD_ZK.info("Znode deleted");

    }

    // Method to disconnect from zookeeper server
    public void close() throws InterruptedException {
        zkeeper.close();
    }



}
